const problem2 = require("./problem2.js");

const inventory = require("./cars.js");

let id = inventory.length

let c = problem2(inventory, id)

console.log(
  ` Last car is a ${c.car_make} ${c.car_model}`
);

